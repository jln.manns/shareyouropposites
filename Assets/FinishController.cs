﻿using System;
using UnityEngine;

public class FinishController : MonoBehaviour
{
    public GameObject playerOne;
    public GameObject playerTwo;

    public bool PlayerOneFinished { get; private set; } = false;

    public bool PlayerTwoFinished { get; private set; } = false;
    // Start is called before the first frame update


    private void OnTriggerEnter2D(Collider2D col)
    {
        GameObject other = col.gameObject;
        if (other.Equals(playerOne))
        {
            Debug.Log("PlayerOne Finished");
            PlayerOneFinished = true;
        }
        if (other.Equals(playerTwo))
        {
            PlayerTwoFinished = true;
            Debug.Log("PlayerTwo Finished");
        }
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        GameObject other = col.gameObject;
        if (other.Equals(playerOne))
        {
            Debug.Log("PlayerOne no longer finished.....");
            PlayerOneFinished = false;
        }
        if (other.Equals(playerTwo))
        {
            PlayerTwoFinished = false;
            Debug.Log("PlayerTwo no longer finished.....");
        }
    }
}
