using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExchangeableEvil : MonoBehaviour
{
    public string key_swap;

    public bool isActive = true;
    public bool isEvil = true;
    public ExchangeableEvil otherPlayer;
    public AudioSource changeEvilSound;

    // Start is called before the first frame update
    void Start()
    {
        if (otherPlayer == null)
        {
            ExchangeableEvil[] comps = GameObject.FindObjectsOfType<ExchangeableEvil>();
            if (comps.Length == 2)
            {
                foreach (var comp in comps)
                {
                    if (comp != GetComponent<ExchangeableEvil>())
                    {
                        otherPlayer = comp;
                    }
                }
            }
        }
        if (otherPlayer != null)
        {
            otherPlayer.otherPlayer = this;
            otherPlayer.isEvil = !isEvil;
            otherPlayer.isActive = isActive;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (otherPlayer != null && Input.GetButtonDown(key_swap))
        {
            changeEvilSound.Play();
            otherPlayer.isEvil = isEvil;
            otherPlayer.GetComponent<ChangeCharacterModel>().changeCharacterModel();

            isEvil = !isEvil;
            GetComponent<ChangeCharacterModel>().changeCharacterModel();
        }
    }
}
