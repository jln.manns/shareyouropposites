using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] private Boolean isEvil = false;
    [SerializeField] private float movementSpeed = 5;
    [SerializeField] private float threatThreshold = 10;
    [SerializeField] private float targetThreshold = 20;
    [SerializeField] private float targetSwitchInterval = 0.5f;
    private bool rotationAdaptedRight = true;
    private bool rotationAdaptedLeft;

    [SerializeField] private Boolean isMoving = true;
    private Vector2 _movementDirection = Vector2.zero;

    private ExchangeableEvil[] players;

    private void Start()
    {
        players = GameObject.FindObjectsOfType<ExchangeableEvil>();
    }

    private float nextTargetSwitch = -1f;
    // Update is called once per frame
    void Update()
    {
        float test = Time.time % 2;
        // Debug.Log(test);
        // Debug.Log("Time: " + Time.time + "and NExt targetSwitch: " + nextTargetSwitch);
        // if (nextTargetSwitch < 0f || Time.time > nextTargetSwitch)
        // {
        //     Debug.Log("Right");
        //     _movementDirection = Vector2.right;
        //     nextTargetSwitch += targetSwitchInterval;
        //     ExchangeableEvil closestThreat = null;
            // foreach (var player in players.Where(p => p.isEvil == this.isEvil))
            // {
            //     if (closestThreat == null || Vector3.Distance(player.gameObject.transform.position, transform.position) <
            //         Vector3.Distance(closestThreat.gameObject.transform.position, transform.position))
            //     {
            //         closestThreat = player;
            //     }
            // }
            // ExchangeableEvil closestTarget = null;
            // foreach (var player in players.Where(p => p.isEvil != this.isEvil))
            // {
            //     if (closestTarget == null || Vector3.Distance(player.gameObject.transform.position, transform.position) <
            //         Vector3.Distance(closestTarget.gameObject.transform.position, transform.position))
            //     {
            //         closestTarget = player;
            //     }
            // }
            // if (closestThreat != null && Vector3.Distance(closestThreat.gameObject.transform.position, transform.position) < threatThreshold)
            // {
            //     _movementDirection = (transform.position - closestThreat.gameObject.transform.position).normalized;
            // }
            // else if (closestTarget != null && Vector3.Distance(closestTarget.gameObject.transform.position, transform.position) < targetThreshold)
            // {
            //     _movementDirection = (closestTarget.gameObject.transform.position - transform.position).normalized;
            // } else
            // {
                // _movementDirection = Vector2.zero;
            // }
        // }
        if (test < 1)
        {
            _movementDirection = Vector2.right;
            if (!rotationAdaptedRight)
            {
                transform.Rotate(0, 180, 0);
                rotationAdaptedLeft = false;
                rotationAdaptedRight = true;
            }
        }
        else
        {
            _movementDirection = Vector2.left;
            if (!rotationAdaptedLeft)
            {
                transform.Rotate(0, 180, 0);
                rotationAdaptedRight = false;
                rotationAdaptedLeft = true;
            }
            
        }
        
    }

    private void FixedUpdate()
    {
        if (isMoving)
        {
            Move();
        }
    }

    private void Move()
    {
        transform.position += new Vector3(_movementDirection.x, _movementDirection.y) * movementSpeed * Time.deltaTime;
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.GetComponent<ExchangeableEvil>().isEvil != isEvil)
        {
            Debug.Log("Oh no Player dead....");
            GameObject.FindObjectOfType<LevelManager>().restartScene();
        }
    }
}
