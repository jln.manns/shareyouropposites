using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CirclingAI : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] private Boolean isEvil = false;
    [SerializeField] private float movementSpeed = 5;
    [SerializeField] private float threatThreshold = 10;
    [SerializeField] private float targetThreshold = 20;
    [SerializeField] private float killThreshold = 1;
    [SerializeField] private float targetSwitchInterval = 0.5f;

    [SerializeField] private Boolean isMoving = true;
    private Vector2 _movementDirection = Vector2.zero;

    private ExchangeableEvil[] players;

    private void Start()
    {
        players = GameObject.FindObjectsOfType<ExchangeableEvil>();
    }

    private float nextTargetSwitch = -1f;
    // Update is called once per frame
    void Update()
    {
        if (nextTargetSwitch < 0f || Time.time > nextTargetSwitch)
        {
            nextTargetSwitch += targetSwitchInterval;
            ExchangeableEvil closestThreat = null;
            foreach (var player in players.Where(p => p.isEvil == this.isEvil))
            {
                if (closestThreat == null || Vector3.Distance(player.gameObject.transform.position, transform.position) <
                    Vector3.Distance(closestThreat.gameObject.transform.position, transform.position))
                {
                    closestThreat = player;
                }
            }
            ExchangeableEvil closestTarget = null;
            foreach (var player in players.Where(p => p.isEvil != this.isEvil))
            {
                if (closestTarget == null || Vector3.Distance(player.gameObject.transform.position, transform.position) <
                    Vector3.Distance(closestTarget.gameObject.transform.position, transform.position))
                {
                    closestTarget = player;
                }
            }
            if (closestTarget != null && Vector3.Distance(closestTarget.gameObject.transform.position, transform.position) < killThreshold)
            {
                GameObject.FindObjectOfType<LevelManager>().restartScene();
            }
            if (closestThreat != null && Vector3.Distance(closestThreat.gameObject.transform.position, transform.position) < threatThreshold)
            {
                if (closestTarget != null && Vector3.Distance(closestTarget.gameObject.transform.position, transform.position) < targetThreshold)
                {
                    Vector3 towards = closestThreat.gameObject.transform.position - transform.position;
                    Vector3 orth = Vector3.Cross(towards, Vector3.forward);
                    Vector3 reference = closestTarget.gameObject.transform.position - closestThreat.gameObject.transform.position;
                    if (Vector3.Dot(orth, reference) > 0)
                    {
                        _movementDirection = orth.normalized;
                    } else
                    {
                        _movementDirection = -orth.normalized;
                    }
                } else
                {
                    _movementDirection = (transform.position - closestThreat.gameObject.transform.position).normalized;
                }
            }
            else if (closestTarget != null && Vector3.Distance(closestTarget.gameObject.transform.position, transform.position) < targetThreshold)
            {
                _movementDirection = (closestTarget.gameObject.transform.position - transform.position).normalized;
            }
            else
            {
                _movementDirection = Vector2.zero;
            }
        }
    }

    private void FixedUpdate()
    {
        if (isMoving)
        {
            Move();
        }
    }

    private void Move()
    {
        transform.position += new Vector3(_movementDirection.x, _movementDirection.y) * movementSpeed * Time.deltaTime;
    }
}
