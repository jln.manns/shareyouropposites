﻿using System;
using UnityEngine;

public class MagnetController : MonoBehaviour
{
    public GameObject playerOne;
    public GameObject playerTwo;
    public int magneticFieldRange = 10;
    public float speed = 0.1f;
    public MagneticType magneticType = MagneticType.Positive;
    
    void Start ()
    {
    }

    private void FixedUpdate()
    {
        ProcessPlayer(playerOne);
        ProcessPlayer(playerTwo);
    }

    private void ProcessPlayer(GameObject player)
    {
        // TODO: Anziehung und Abtoßen verhalten basierend auf Pol des Spielers...
        float distance = Vector3.Distance(transform.position, player.transform.position);
        if (distance < magneticFieldRange)
        {
            player.GetComponent<PlayerController>().inMagnaticField = true;
            
            // Debug.Log("Distance: " + disctance);
            float absoluteDistance = magneticFieldRange - distance;
            float step = absoluteDistance * speed;
            // Debug.Log("STep: " + step);
            
            if (player.GetComponent<ExchangeableMagnetic>().isMagnetic)
            {
                Vector3 magnetFieldDirection = (transform.position - player.transform.position).normalized;
                // Debug.Log("1Player ist magnetic with step : " + (magnetField.normalized * step));
                player.GetComponent<PlayerController>().MagneticForce = ((magnetFieldDirection * step));
                Debug.DrawLine(transform.position, transform.position + (magnetFieldDirection * step),Color.red);

                
            }

            if (!player.GetComponent<ExchangeableMagnetic>().isMagnetic)
            {
                //Anziehen
                Vector3 magnetFieldDirection = (player.transform.position-transform.position).normalized;
                // Vector2 force = new Vector2(-magneticFieldRange, 0) - magnetField;
                // Debug.Log("Magneticfield : " + (magnetField.normalized));
                // Debug.Log("Magneticfield with force old : " + (magnetField.normalized * step));
                player.GetComponent<PlayerController>().MagneticForce = (magnetFieldDirection * step);
                Debug.DrawLine(transform.position, transform.position + (magnetFieldDirection * step));
                
                
            }
        }
        else
        {
            player.GetComponent<PlayerController>().MagneticForce = new Vector2();
            player.GetComponent<PlayerController>().inMagnaticField = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        // GameObject other = col.gameObject;
        Debug.Log("Entered magnetic field....");
        // other.GetComponent<PlayerController>();
    }

}
