using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeCharacterModel : MonoBehaviour
{
    [SerializeField] private GameObject BigMinusGood;
    [SerializeField] private GameObject BigPlusGood;
    [SerializeField] private GameObject SmallMinusGood;
    [SerializeField] private GameObject SmallPlusGood;
    
    [SerializeField] private GameObject BigMinusBad;
    [SerializeField] private GameObject BigPlusBad;
    [SerializeField] private GameObject SmallMinusBad;
    [SerializeField] private GameObject SmallPlusBad;

    private void Awake()
    {
        changeCharacterModel();
    }
    
    public void changeCharacterModel()
    {
        bool isEvil = GetComponent<ExchangeableEvil>().isEvil;
        bool isMinus = GetComponent<ExchangeableMagnetic>().isMagnetic;
        bool isBig = GetComponent<ExchangeableSize>().isBig;
        //bool isBig, bool isMinus, bool isGood
        GameObject goToModel;
        if (isBig && isMinus && !isEvil)
        {
            goToModel = BigMinusGood;
        }
        else if(isBig && !isMinus &&!isEvil)
        {
            goToModel = BigPlusGood;
        }else if (!isBig && isMinus && !isEvil)
        {
            goToModel = SmallMinusGood;
        }else if (!isBig && !isMinus && !isEvil)
        {
            goToModel = SmallPlusGood;
        }
        else if (isBig && isMinus && isEvil)
        {
            goToModel = BigMinusBad;
        }
        else if(isBig && !isMinus && isEvil)
        {
            goToModel = BigPlusBad;
        }else if (!isBig && isMinus && isEvil)
        {
            goToModel = SmallMinusBad;
        }else
        {
            goToModel = SmallPlusBad;
        }
        var children = new List<GameObject>();
        foreach (Transform child in transform) children.Add(child.gameObject);
        children.ForEach(child => Destroy(child));
        Instantiate(goToModel.transform.gameObject,transform);
        
    }
}
