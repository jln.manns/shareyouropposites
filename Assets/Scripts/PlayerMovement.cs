using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public const float movementSpeed = 10f;
    public const float jumpSpeed = 20f;
    public const float jumpDecay = 2f;

    public string key_moveRight = "d";
    public string key_moveLeft = "a";
    public string key_jump = "space";

    private Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }


    
    // Update is called once per frame
    void Update()
    {
        jumpStrength = Mathf.Max(0f, jumpStrength - Time.deltaTime * jumpDecay);
        horizontal = Input.GetKey(key_moveRight) ? 1 : 0 - (Input.GetKey(key_moveLeft) ? 1 : 0);
        if (Input.GetKeyDown(key_jump))
        {
            isJumping = true;
            jumpStrength = 1f;
        }
        if (Input.GetKeyUp(key_jump))
        {
            isJumping = false;
            jumpStrength = 0f;
        }
    }

    private float horizontal = 0;

    private bool isJumping = false;
    private float jumpStrength = 0;
    private void FixedUpdate()
    {
        float x = 0f, y = 0f;
        x = horizontal * movementSpeed;
        if (!isJumping | jumpStrength == 0)
        {
            y = Mathf.Min(rb.velocity.y, 0f);
        } else
        {
            y = jumpStrength * jumpSpeed;
        }
        rb.velocity = new Vector2(x,y);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("boom");
    }
}
