using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class CameraSplitScreenController : MonoBehaviour
{
    [SerializeField] private Transform player1;

    [SerializeField] private Transform player2;
    [SerializeField] private Camera secondaryCamera;
    [SerializeField] private Camera boundryCamera;

    private Vector3 _pointBetweenCharacters;
    private bool _bothPlayersInRange = true;
    public float maxXDistance = 5;
    
    // Start is called before the first frame update

    private void Awake()
    {
        _pointBetweenCharacters = CalculateMidPoint();
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        _bothPlayersInRange = Math.Abs(player1.transform.position.x - player2.transform.position.x) < maxXDistance;
        var mainCamTransform = transform;
        var mainCamPosition = mainCamTransform.position;

        if (_bothPlayersInRange)
        {   
            boundryCamera.gameObject.SetActive(false);
            secondaryCamera.gameObject.SetActive(false);
            if (Camera.main != null) Camera.main.rect = new Rect(0, 0, 1f, 1);
            _pointBetweenCharacters = CalculateMidPoint();
            mainCamTransform.position =
                new Vector3(_pointBetweenCharacters.x, mainCamPosition.y, mainCamPosition.z);
        }
        else
        {
            handleSplitCamera();
            
        }
    }

    private void handleSplitCamera()
    {
        boundryCamera.gameObject.SetActive(true);

        var mainCamPosition = transform.position;
        var secondaryCameraPosition = secondaryCamera.transform.position;
        var player1Position = player1.position;
        var player2Position = player2.position;
        
        var leftPlayer = player1Position.x < player2Position.x ? player1 : player2;;
        var rightPlayer = player1Position.x < player2Position.x ? player2 : player1;;
        
       
        transform.position = new Vector3(rightPlayer.position.x, mainCamPosition.y, mainCamPosition.z);
        if (Camera.main != null) Camera.main.rect = new Rect(0.502f, 0, 0.498f, 1);
        secondaryCamera.transform.position = new Vector3(leftPlayer.position.x, mainCamPosition.y, secondaryCameraPosition.z); 
        secondaryCamera.gameObject.SetActive(true);
    }

    private Vector3 CalculateMidPoint()
    {
        Vector3 midPoint = (player2.position + player1.position)/2;
        return midPoint;
    }
}
