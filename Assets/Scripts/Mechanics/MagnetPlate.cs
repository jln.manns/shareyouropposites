using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagnetPlate : MonoBehaviour
{

    private void OnTriggerEnter2D(Collider2D col)
    {
        Debug.Log("Hi");
        ExchangeableMagnetic mag = col.gameObject.GetComponent<ExchangeableMagnetic>();
        if (mag != null)
        {
            Debug.Log("ziiiiip");
            Debug.Log(mag.gameObject.name + ": " + mag.isMagnetic);
            mag.isUnderMagnetPlate = true;
        }
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        Debug.Log("Bye");
        ExchangeableMagnetic mag = col.gameObject.GetComponent<ExchangeableMagnetic>();
        if (mag != null)
        {
            Debug.Log("zooooop");
            Debug.Log(mag.gameObject.name + ": " + mag.isMagnetic);
            mag.isUnderMagnetPlate = false;
        }
    }
}
