using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Killbox : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D col)
    {
        PlayerController pc = col.gameObject.GetComponent<PlayerController>();
        if (pc != null)
        {
            Debug.Log("Dead");
            GameObject.FindObjectOfType<LevelManager>().restartScene();
        }
    }
}
