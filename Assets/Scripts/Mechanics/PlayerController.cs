﻿using UnityEngine;

//namespace Platformer.Mechanics
//{
/// <summary>
/// This is the main class used to implement control of the player.
/// It is a superset of the AnimationController class, but is inlined to allow for any kind of customisation.
/// </summary>
public class PlayerController : KinematicObject
{
    //public AudioClip jumpAudio;
    //public AudioClip respawnAudio;
    //public AudioClip ouchAudio;
    public string moveAxis, jumpKey;

    /// <summary>
    /// Max horizontal speed of the player.
    /// </summary>
    public float maxSpeed = 7;
    /// <summary>
    /// Initial jump velocity at the start of a jump.
    /// </summary>
    public float jumpTakeOffSpeed = 10;
    public float jumpTakeOffSpeedSmall = 7.5F;

    public JumpState jumpState = JumpState.Grounded;
    private bool stopJump;
    /*internal new*/
    public Collider2D collider2d;
    ///*internal new*/ public AudioSource audioSource;
    //public Health health;
    public bool controlEnabled = true;

    bool jump;
    Vector2 move;

    private Animator _animator;
    private Transform playerTransform;

    public AudioSource jumpSoundSmall;
    public AudioSource jumpSoundBig;

    [SerializeField] private Transform modelTransform;
    
    Vector2 _magneticForce = new Vector2();

    public Vector2 MagneticForce
    {
        get => _magneticForce;
        set => _magneticForce = value;
    }


    //SpriteRenderer spriteRenderer;
    //internal Animator animator;
    //readonly PlatformerModel model = Simulation.GetModel<PlatformerModel>();

    public Bounds Bounds => collider2d.bounds;

    void Awake()
    {
        //health = GetComponent<Health>();
        //audioSource = GetComponent<AudioSource>();
        //collider2d = GetComponent<Collider2D>();
        //spriteRenderer = GetComponent<SpriteRenderer>();
        _animator = GetComponentInChildren<Animator>();
        playerTransform = transform;
    }

    protected override void Update()
    {
        //modelTransform.localScale = new Vector3(modelTransform.localScale.x,
        //                                        magneticData.isMagnetic && magneticData.isUnderMagnetPlate ? -0.1f : 0.1f,
        //                                        modelTransform.localScale.z);
        if (_animator == null)
        {
            _animator = GetComponentInChildren<Animator>();
        }
        
        if (controlEnabled)
        {
            move.x = Input.GetAxis(moveAxis);
            if (jumpState == JumpState.Grounded && Input.GetButtonDown(jumpKey))
                jumpState = JumpState.PrepareToJump;
            else if (Input.GetButtonUp(jumpKey))
            {
                stopJump = true;
                //Schedule<PlayerStopJump>().player = this;
            }
        }
        else
        {
            move.x = 0;
        }
        UpdateJumpState();
        base.Update();
        if (jumpState != JumpState.Grounded && jumpState != JumpState.Landed)
        {
            _animator.SetFloat("Speed", 0.33333f);

        }

        // Debug.Log("animatorSpeed: "+_animator.GetFloat("Speed")+"    JumpState: "+ jumpState);

    }

    void UpdateJumpState()
    {
        jump = false;
        switch (jumpState)
        {
            case JumpState.PrepareToJump:
                jumpState = JumpState.Jumping;
                jump = true;
                stopJump = false;
                if (GetComponent<ExchangeableSize>().isBig)
                {
                    jumpSoundBig.Play();
                }
                else
                {
                    jumpSoundSmall.Play();
                }
                break;
            case JumpState.Jumping:
                if (!IsGrounded)
                {
                    //Schedule<PlayerJumped>().player = this;
                    jumpState = JumpState.InFlight;

                }
                break;
            case JumpState.InFlight:
                if (IsGrounded)
                {
                    //Schedule<PlayerLanded>().player = this;
                    jumpState = JumpState.Landed;

                }
                break;
            case JumpState.Landed:
                jumpState = JumpState.Grounded;
                break;
        }
    }

    protected override void ComputeVelocity()
    {
        if (jump && IsGrounded)
        {
            var jumpSpeed = 0F;
            if (GetComponent<ExchangeableSize>().isBig)
            {
                jumpSpeed = GetGravityMult() * jumpTakeOffSpeed;
            }
            else
            {
                jumpSpeed = GetGravityMult() * jumpTakeOffSpeedSmall;
            }

            velocity.y = jumpSpeed * 0.9f;
            jump = false;
        }
        else if (stopJump)
        {
            stopJump = false;
            if (velocity.y > 0)
            {
                velocity.y = velocity.y * 0f;
            }
        }

        if (move.x > 0.01f)
        {
            var oldRotation = playerTransform.rotation;
            playerTransform.localRotation = new Quaternion(oldRotation.x, 0, oldRotation.z, oldRotation.w);
            _animator.SetFloat("Speed", 1f);
        }
        else if (move.x < -0.01f)
        {
            var oldRotation = playerTransform.rotation;
            playerTransform.localRotation = new Quaternion(oldRotation.x, 180, oldRotation.z, oldRotation.w);
            _animator.SetFloat("Speed", 1f);
        }
        else
        {
            
            _animator.SetFloat("Speed", 0f);
        }

        //animator.SetBool("grounded", IsGrounded);
        //animator.SetFloat("velocityX", Mathf.Abs(velocity.x) / maxSpeed);
        if (!inMagnaticField)
        {
            targetVelocity = (move + _magneticForce) * maxSpeed;
        } else
        {
            targetVelocity = (move + _magneticForce).normalized * maxSpeed;
        }
    }

    public enum JumpState
    {
        Grounded,
        PrepareToJump,
        Jumping,
        InFlight,
        Landed
    }
}
//}
