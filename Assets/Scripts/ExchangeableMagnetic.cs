using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExchangeableMagnetic : MonoBehaviour
{
    public string key_swap;

    public bool isActive = true;
    public bool isMagnetic = true;
    public bool isUnderMagnetPlate = false;
    public ExchangeableMagnetic otherPlayer;
    public AudioSource magneticSound;

    // Start is called before the first frame update
    void Start()
    {
        if (otherPlayer == null)
        {
            ExchangeableMagnetic[] comps = GameObject.FindObjectsOfType<ExchangeableMagnetic>();
            if (comps.Length == 2)
            {
                foreach (var comp in comps)
                {
                    if (comp != GetComponent<ExchangeableMagnetic>())
                    {
                        otherPlayer = comp;
                    }
                }
            }
        }
        if (otherPlayer != null)
        {
            otherPlayer.otherPlayer = this;
            otherPlayer.isMagnetic = !isMagnetic;
            otherPlayer.isActive = isActive;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (otherPlayer != null && Input.GetButtonDown(key_swap))
        {
            otherPlayer.isMagnetic = isMagnetic;
            otherPlayer.GetComponent<ChangeCharacterModel>().changeCharacterModel();
            isMagnetic = !isMagnetic;
            magneticSound.Play();
            GetComponent<ChangeCharacterModel>().changeCharacterModel();
        }
    }
}
