using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExchangeableSize : MonoBehaviour
{
    public float growFactor = 2f;
    public float shrinkFactor = 0.5f;
    public string key_swap;

    public bool isActive = true;
    public bool isBig = true;
    public ExchangeableSize otherPlayer;

    public Collider2D resizeCollider;
    private Vector3 colliderOriginalScale;
    public AudioSource changeSound;

    // Start is called before the first frame update
    void Start()
    {
        if (resizeCollider != null)
        {
            colliderOriginalScale = resizeCollider.transform.localScale;
        }
        if (otherPlayer == null)
        {
            ExchangeableSize[] comps = GameObject.FindObjectsOfType<ExchangeableSize>();
            if (comps.Length == 2)
            {
                foreach (var comp in comps)
                {
                    if (comp != GetComponent<ExchangeableSize>())
                    {
                        otherPlayer = comp;
                    }
                }
            }
        }
        if (otherPlayer != null)
        {
            otherPlayer.otherPlayer = this;
            otherPlayer.isBig = !isBig;
            otherPlayer.isActive = isActive;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (otherPlayer != null && Input.GetButtonDown(key_swap))
        {
            changeSound.Play();
            otherPlayer.isBig = isBig;
            otherPlayer.GetComponent<ChangeCharacterModel>().changeCharacterModel();
            isBig = !isBig;
            GetComponent<ChangeCharacterModel>().changeCharacterModel();
        }
        if (resizeCollider != null)
        {
            if (otherPlayer != null & isActive)
            {
                resizeCollider.transform.localScale = (isBig ? growFactor : shrinkFactor) * colliderOriginalScale;
            } else
            {
                resizeCollider.transform.localScale = colliderOriginalScale;
            }
        }
    }
}
