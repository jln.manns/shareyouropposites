﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public GameObject finishDoor;
    public int maxLevel = 2;


    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        FinishController finishController =  finishDoor.GetComponent<FinishController>();
        // Oder Finish doch lieber auf dem Spieler mappen?
        if (finishController.PlayerOneFinished && finishController.PlayerTwoFinished)
        {
            string sceneName = SceneManager.GetActiveScene().name;
            var sceneNameSplit = sceneName.Split('_');
            int level = Int32.Parse(sceneNameSplit[1]);
            string nextLevel = "MainMenu";
            if (level < maxLevel)
            {
                level++;
                nextLevel = "Level_" + level;
            }

            StartCoroutine(DelayLoadLevel(nextLevel));
        }
    }

    IEnumerator DelayLoadLevel(string nextLevel)
    {
        GameObject.Find("TransitionToNextScene").GetComponent<Animator>().SetTrigger("TriggerTransition");
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(nextLevel);
    }

    public void restartScene()
    {
        StartCoroutine(DelayRestartLevel());
    }

    IEnumerator DelayRestartLevel()
    {
        GameObject.Find("TransitionOnDeath").GetComponent<Animator>().SetTrigger("DeathTransition");
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
