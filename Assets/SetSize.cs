using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetSize : MonoBehaviour
{
    [SerializeField] private ExchangeableSize x;
    [SerializeField] private Sprite s1;
    [SerializeField] private Sprite s2;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (x.isBig)
            GetComponent<Image>().sprite = s1;
        else
            GetComponent<Image>().sprite = s2;
    }
}
