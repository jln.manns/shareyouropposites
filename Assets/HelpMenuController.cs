using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HelpMenuController : MonoBehaviour
{
    public void GoToMainMenu()
    {
        Debug.Log("Select Main Menu:)");
        SceneManager.LoadScene("Scenes/MainMenu");
    }
}
