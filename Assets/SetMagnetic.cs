using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetMagnetic : MonoBehaviour
{
    [SerializeField] private ExchangeableMagnetic x;
    [SerializeField] private Sprite s1;
    [SerializeField] private Sprite s2;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (!x.isMagnetic)
            GetComponent<Image>().sprite = s1;
        else
            GetComponent<Image>().sprite = s2;
    }
}
